@echo off

set cd_path=%~dp0
set sh_path=%cd_path%workspace\start.sh

if not exist %sh_path% (
  vagrant up --provider virtualbox
  vagrant halt
  vagrant plugin install vagrant-teraterm
  echo .
  echo インストール終了
  echo 何かキーを押してください
) else (
  echo インストール済みです
)



pause > nul