#
# Cookbook Name:: ruby
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

git "/home/vagrant/.rbenv" do
  repository "git://github.com/sstephenson/rbenv.git"
  reference "master"
  action :checkout
  user "vagrant"
  group "vagrant"
end

directory "/home/vagrant/.rbenv/plugins/" do
  owner "vagrant"
  user "vagrant"
  group "vagrant"
  action :create
end

git "/home/vagrant/.rbenv/plugins/ruby-build" do
  repository "git://github.com/sstephenson/ruby-build.git"
  reference "master"
  action :checkout
  user "vagrant"
  group "vagrant"
end

template ".bash_profile" do
  source ".bash_profile.erb"
  path "/home/vagrant/.bash_profile"
  mode 0644
  owner "vagrant"
  group "vagrant"
  not_if "grep rbenv ~/.bash_profile", :environment => { :'HOME' => "/home/vagrant" }
end

bash "install ruby" do
  user "vagrant"
  group "vagrant"
  environment "HOME" => "/home/vagrant"
  code <<-EOC
    /home/vagrant/.rbenv/bin/rbenv install 2.3.0
    /home/vagrant/.rbenv/bin/rbenv global 2.3.0
  EOC
  creates "/home/vagrant/.rbenv/versions/2.3.0"
end
