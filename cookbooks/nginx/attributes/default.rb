
default["nginx"]["user"]         = "nginx"
default["nginx"]["group"]        = "nginx"
default["nginx"]["version"]      = "1.10.0"
default["nginx"]["conf_path"]    = "/etc/nginx"
default["nginx"]["log_path"]     = "/var/log/nginx"
default["nginx"]["pid_path"]     = "/var/run"
default["nginx"]["lock_path"]    = "/var/run"
default["nginx"]["prefix_path"]  = "/usr/local/nginx"
default["nginx"]["sbin_path"]    = "/usr/local/nginx/sbin"
