#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "install_nginx" do
  user "root"
  group "root"
  code <<-EOC
    useradd -M -s /sbin/nologin #{node["nginx"]["user"]}
    wget http://nginx.org/download/nginx-#{node["nginx"]["version"]}.tar.gz
    tar zxvf nginx-#{node["nginx"]["version"]}.tar.gz
    cd nginx-#{node["nginx"]["version"]}
    ./configure \
    --conf-path=#{node["nginx"]["conf_path"]}/nginx.conf \
    --error-log-path=#{node["nginx"]["log_path"]}/error.log \
    --http-log-path=#{node["nginx"]["log_path"]}/access.log \
    --user=#{node["nginx"]["user"]} \
    --group=#{node["nginx"]["group"]} \
    --pid-path=#{node["nginx"]["pid_path"]}/nginx.pid \
    --lock-path=#{node["nginx"]["lock_path"]}/nginx.lock
    make
    make install
    cd ../
    rm -rf nginx-#{node["nginx"]["version"]}*
  EOC
  creates node["nginx"]["prefix_path"]
end

template "nginx.conf" do
  path "#{node["nginx"]["conf_path"]}/nginx.conf"
  source "nginx.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

template "nginx.service" do
  path "/etc/systemd/system/nginx.service"
  source "nginx.service.erb"
  owner "root"
  group "root"
  mode 0655
end

bash "enable_nginx" do
  user "root"
  group "root"
  code <<-EOC
    systemctl start nginx
    systemctl enable nginx
  EOC
  creates "/etc/systemd/system/multi-user.target.wants/nginx.service"
end
