#
# Cookbook Name:: nodejs
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "inall_nodejs" do
  user "vagrant"
  group "vagrant"
  code <<-EOC
    wget https://nodejs.org/dist/v4.4.0/node-v4.4.0.tar.gz -P /home/vagrant
    cd /home/vagrant
    tar zxvf node-v4.4.0.tar.gz
    cd node-v4.4.0
    ./configure
    make
    sudo make install
    cd ../
    rm -rf node-v4.4.0*
  EOC
  creates "/usr/local/bin/node"
end
